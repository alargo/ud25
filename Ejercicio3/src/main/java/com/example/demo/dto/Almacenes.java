package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Almacenes")
public class Almacenes {

	//Atributos de entidad cliente
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "codigo")
		private int codigo;
		@Column(name = "lugar")
		private String lugar;
		@Column(name = "capacidad")
		private int capacidad;

		
	    @OneToMany
	    @JoinColumn(name="numreferencia")
	    private List<Cajas> cajas;


	    public Almacenes() {
			
		}
	    

		public Almacenes(int codigo, String lugar, int capacidad, List<Cajas> cajas) {
			super();
			this.codigo = codigo;
			this.lugar = lugar;
			this.capacidad = capacidad;
			this.cajas = cajas;
		}


		public int getCodigo() {
			return codigo;
		}


		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}


		public String getLugar() {
			return lugar;
		}


		public void setLugar(String lugar) {
			this.lugar = lugar;
		}


		public int getCapacidad() {
			return capacidad;
		}


		public void setCapacidad(int capacidad) {
			this.capacidad = capacidad;
		}

		@JsonIgnore
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "Cajas")
		public List<Cajas> getCajas() {
			return cajas;
		}


		public void setCajas(List<Cajas> cajas) {
			this.cajas = cajas;
		}


		@Override
		public String toString() {
			return "Almacenes [codigo=" + codigo + ", lugar=" + lugar + ", capacidad=" + capacidad + ", cajas=" + cajas
					+ "]";
		}
		
		
	    
		
		
		
		

		
	
}