package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ICajasDAO;
import com.example.demo.dto.Cajas;

@Service
public class CajasServiceImpl implements ICajasService {
	//Utilizamos los metodos de la interface IClienteDAO, es como si instaciaramos.
	@Autowired
	ICajasDAO iCajaDAO;
	
	@Override
	public List<Cajas> listarCajas() {
		
		return iCajaDAO.findAll();
	}

	@Override
	public Cajas guardarCaja(Cajas cajas) {
		
		return iCajaDAO.save(cajas);
	}

	@Override
	public Cajas cajaXNumReferencia(String numReferencia) {
		
		return iCajaDAO.findById(numReferencia).get();
	}

	@Override
	public Cajas actualizarCaja(Cajas cajas) {
		
		return iCajaDAO.save(cajas);
	}

	@Override
	public void eliminarCaja(String numReferencia) {
		
		iCajaDAO.deleteById(numReferencia);
		
	}
}
