package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IAlmacenesDAO;
import com.example.demo.dto.Almacenes;


@Service
public class AlmacenesServiceImpl implements IAlmacenesService {

	//Utilizamos los metodos de la interface IClienteDAO, es como si instaciaramos.
	@Autowired
	IAlmacenesDAO iAlmacenDAO;
	
	@Override
	public List<Almacenes> listarAlmacenes() {
		
		return iAlmacenDAO.findAll();
	}

	@Override
	public Almacenes guardarAlmacen(Almacenes almacen) {
		
		return iAlmacenDAO.save(almacen);
	}

	@Override
	public Almacenes almacenXCodigo(int codigo) {
		
		return iAlmacenDAO.findById(codigo).get();
	}

	@Override
	public Almacenes actualizarAlmacen(Almacenes almacen) {
		
		return iAlmacenDAO.save(almacen);
	}

	@Override
	public void eliminarAlmacen(int codigo) {
		
		iAlmacenDAO.deleteById(codigo);
		
	}
}
