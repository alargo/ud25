package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IEmpleadosDAO;
import com.example.demo.dto.Empleados;

@Service
public class EmpleadosServiceImpl implements IEmpleadosService {
	//Utilizamos los metodos de la interface IClienteDAO, es como si instaciaramos.
	@Autowired
	IEmpleadosDAO iEmpleadoDAO;
	
	@Override
	public List<Empleados> listarEmpleados() {
		
		return iEmpleadoDAO.findAll();
	}

	@Override
	public Empleados guardarEmpleado(Empleados empleado) {
		
		return iEmpleadoDAO.save(empleado);
	}

	@Override
	public Empleados empleadoXDNI(String dni) {
		
		return iEmpleadoDAO.findById(dni).get();
	}

	@Override
	public Empleados actualizarEmpleado(Empleados empleado) {
		
		return iEmpleadoDAO.save(empleado);
	}

	@Override
	public void eliminarEmpleado(String dni) {
		
		iEmpleadoDAO.deleteById(dni);
		
	}
}
