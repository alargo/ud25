package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Departamentos;
import com.example.demo.service.DepartamentosServiceImpl;



@RestController
@RequestMapping("/api")
public class DepartamentosController {
	@Autowired
	DepartamentosServiceImpl departamentoServideImpl;
	
	@GetMapping("/departamentos")
	public List<Departamentos> listarDepartamentos(){
		return departamentoServideImpl.listarDepartamentos();
	}
	
	@PostMapping("/departamentos")
	public Departamentos salvarDepartamento(@RequestBody Departamentos departamento) {
		
		return departamentoServideImpl.guardarDepartamento(departamento);
	}
	
	@GetMapping("/departamentos/{codigo}")
	public Departamentos departamentoXCodigo(@PathVariable(name="codigo") int codigo) {
		
		Departamentos departamento_xCodigo= new Departamentos();
		
		departamento_xCodigo=departamentoServideImpl.departamentoXCodigo(codigo);
		
		System.out.println("Departamento XCodigo: "+departamento_xCodigo);
		
		return departamento_xCodigo;
	}
	
	@PutMapping("/departamentos/{codigo}")
	public Departamentos actualizarDepartamento(@PathVariable(name="codigo")int codigo,@RequestBody Departamentos departamento) {
		
		Departamentos departamento_seleccionado= new Departamentos();
		Departamentos departamento_actualizado= new Departamentos();
		
		departamento_seleccionado= departamentoServideImpl.departamentoXCodigo(codigo);
		
		departamento_seleccionado.setNombre(departamento.getNombre());
		departamento_seleccionado.setpresupuesto(departamento.getpresupuesto());

		
		departamento_actualizado = departamentoServideImpl.actualizarDepartamento(departamento_seleccionado);
		
		System.out.println("El departamento actualizado es: "+ departamento_actualizado);
		
		return departamento_actualizado;
	}
	
	@DeleteMapping("/departamentos/{codigo}")
	public void eleiminarDepartamento(@PathVariable(name="codigo")int codigo) {
		departamentoServideImpl.eliminarDepartamento(codigo);
	}
	
	
}
