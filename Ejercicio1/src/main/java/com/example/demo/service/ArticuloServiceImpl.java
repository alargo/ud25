package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IArticuloDAO;
import com.example.demo.dto.Articulo;

@Service
public class ArticuloServiceImpl implements IArticuloService{
	
	@Autowired
	IArticuloDAO iArticuloDao;
	
	@Override
	public List<Articulo> listarArticulos() {
		return iArticuloDao.findAll();
	}

	@Override
	public Articulo guardarArticulo(Articulo articulo) {
		return iArticuloDao.save(articulo);
	}

	@Override
	public Articulo articuloXID(int id) {
		return iArticuloDao.findById(id).get();
	}

	@Override
	public Articulo actualizarArticulo(Articulo articulo_seleccionado) {
		return iArticuloDao.save(articulo_seleccionado);
	}

	@Override
	public void eliminarArticulo(int id) {
		iArticuloDao.deleteById(id);
		
	}

}
