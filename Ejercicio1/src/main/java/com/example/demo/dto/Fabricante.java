package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="fabricantes")
public class Fabricante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="codigo")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@OneToMany
    @JoinColumn(name="codigo")
    private List<Articulo> articulo;

	/**
	 * 
	 */
	public Fabricante() {
	}

	/**
	 * @param id
	 * @param nombre
	 * @param articulo
	 */
	public Fabricante(int id, String nombre, List<Articulo> articulo) {
		this.id = id;
		this.nombre = nombre;
		this.articulo = articulo;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return id;
	}

	/**
	 * @param id the codigo to set
	 */
	public void setCodigo(int id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @param articulo the articulo to set
	 */
	public void setArticulo(List<Articulo> articulo) {
		this.articulo = articulo;
	}
	
	/**
	 * @return the articulo
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "articulo")
	public List<Articulo> getArticulo() {
		return articulo;
	}

	@Override
	public String toString() {
		return "Fabricante [id=" + id + ", nombre=" + nombre + ", articulo=" + articulo + "]";
	}

	
	
	
}
