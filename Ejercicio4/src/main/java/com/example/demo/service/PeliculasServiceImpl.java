package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IPeliculasDAO;
import com.example.demo.dto.Peliculas;

@Service
public class PeliculasServiceImpl implements IPeliculasService {
	//Utilizamos los metodos de la interface IPeliculasDAO, es como si instaciaramos.
	@Autowired
	IPeliculasDAO iPeliculasDAO;
	
	@Override
	public List<Peliculas> listarPeliculas() {
		
		return iPeliculasDAO.findAll();
	}

	@Override
	public Peliculas guardarPeliculas(Peliculas peliculas) {
		
		return iPeliculasDAO.save(peliculas);
	}

	@Override
	public Peliculas peliculasXCodigo(int codigo) {
		
		return iPeliculasDAO.findById(codigo).get();
	}

	@Override
	public Peliculas actualizarPeliculas(Peliculas peliculas) {
		
		return iPeliculasDAO.save(peliculas);
	}

	@Override
	public void eliminarPeliculas(int codigo) {
		
		iPeliculasDAO.deleteById(codigo);
		
	}
}
